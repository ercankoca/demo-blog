
@extends('layouts.app')

@section('content')


<div class="container" style="padding-top: 25px">


	<div class="panel panel-default">
  <div class="panel-heading">
  	@foreach($kurumsal as $row)
    <h3 class="panel-title">{{ $row->title }} </h3>
  </div>
  <div class="panel-body">


<p>{{$row->content}}</p>

@endforeach
  </div>
</div>


</div>

@endsection