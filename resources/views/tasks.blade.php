@extends('layouts.app')

@section('content')

 <div class="logo"><a href="index-2.html"><h1><span><img src="{{URL :: to('') }}/images/kucukpazar_logo.png"></span></h1></a></div>


 <section id="line-slider">
            <div class="leoslider">
              <ul class="slides">
               <li>
              <a href="kampanyalar.html"><img src="{{URL :: to('') }}/images/1.jpg" alt="Haziran Ayı İndirim Kataloğu " /></a></li>
                                               <li>
                   <img src="{{URL :: to('') }}/images/ramazan1.jpg" alt=" " />
                </li>
                <li>
                 <img src="{{URL :: to('') }}/images/ramazan2.jpg" alt=" " />
                </li>
                <li>
                 <img src="{{URL :: to('') }}/images/7.jpg" alt=" " />
                </li>
                <li>
               <img src="{{URL :: to('') }}/images/3.jpg" alt=" " />
                </li>
                <li>
                 <img src="{{URL :: to('') }}/images/4.jpg" alt=" " />
                </li>
                <li>
                 <img src="{{URL :: to('') }}/images/8.jpg" alt=" " />
              </ul>
            </div>
        </section>
        <!-- /Line Slider -->

<div class="banner-bottom">
                <!--//screen-gallery-->
                        <div class="sreen-gallery-cursual">
                             <!-- required-js-files-->
                            <link href="{{URL :: to('') }}/css/owl.carousel.css" rel="stylesheet">
                                <script src="{{URL :: to('') }}/js/owl.carousel.js"></script>
                                    <script>
                                $(document).ready(function() {
                                  $("#owl-demo").owlCarousel({
                                    items :5,
                                    lazyLoad : true,
                                    autoPlay : true,
                                    navigation :true,
                                    navigationText :  false,
                                    pagination : true,
                                  });
                                });
                                </script>
                                 <!--//required-js-files-->
                               <div id="owl-demo" class="owl-carousel">
                                  <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s5.jpg" class="img-responsive" alt=""/>
                                              <h5>Plastik Mutfak Gereçleri</h5>
                                            </div>
                                 </div>
                                     <div class="item-owl">
                                        <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s6.jpg" class="img-responsive" alt=""/>
                                              <h5>Züccaciye</h5>
                                       </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s2.jpg" class="img-responsive" alt=""/>
                                              <h5>Banyo Aksesuarları</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s1.jpg" class="img-responsive" alt=""/>
                                              <h5>Hediyelik Eşya</h5>
                                            </div>
                                    </div>
                                    <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s4.jpg" class="img-responsive" alt=""/>
                                              <h5>Balkon&amp;Bahçe</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s3.jpg" class="img-responsive" alt=""/>
                                              <h5>Hırdavat</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s7.jpg" class="img-responsive" alt=""/>
                                              <h5>Plastik Banyo Gereçleri</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                        <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s8.jpg" class="img-responsive" alt=""/>
                                            <h5>Ev Elektroniği</h5>
                                       </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s9.jpg" class="img-responsive" alt=""/>
                                              <h5>Kozmetik</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s10.jpg" class="img-responsive" alt=""/>
                                              <h5>Kırtasiye</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="{{URL :: to('') }}/images/s11.jpg" class="img-responsive" alt=""/>
                                              <h5>Oyuncak</h5>
                                            </div>
                                            
                                    </div>
                              </div>
                        <!--//screen-gallery-->

          </div>
 </div>

<!--news-->

<div class="projects">
  <div class="container">
    <h3 class="tittle">İHTİYACINIZ OLAN NE VARSA <span>KÜÇÜKPAZAR'DA!</span></h3>
      <div class="projects-inner">
        <div class="col-md-7 banner-slider">
                            <div class="callbacks_container">
                                <ul class="rslides" id="slider3">
                                    <li>
                                      <div class="blog-img">
                                         <img src="{{URL :: to('') }}/images/pro1.jpg" class="img-responsive" alt="" />
                                      </div>
                                
                                    </li>
                                    <li>
                                      <div class="blog-img">
                                        <img src="{{URL :: to('') }}/images/pro2.jpg" class="img-responsive" alt="" />
                                      </div>
                                
                                  </li>
                                  
                                </ul>
                          </div>
        </div>
                    <div class="col-md-5 ban-text">
                       <div class="choose">
                            <div class="choose_img">
                                 <h3>Neden Biz ?</h3>
                                   <!-- choose icon -->
                                   <div class="choose_icon">
                                        <div class="choose_left">
                                            <span class="glyphicon glyphicon-home"></span>
                                        </div>
                                            <div class="choose_right">
                                                <p>Zengin ürün çeşidi,</p>
                                            </div>
                                        <div class="clearfix"></div>
                              </div>
                                 
                                  <!-- choose icon -->
                                  <div class="choose_icon">
                                    <div class="choose_left">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </div>
                                    <div class="choose_right">
                                        <p>En yeni ürünler en uygun fiyatlarla</p>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div>
                                 
                                  <!-- choose icon -->
                                  <div class="choose_icon">
                                    <div class="choose_left">
                                        <span class="glyphicon glyphicon-heart-empty"></span>
                                    </div>
                                    <div class="choose_right">
                                        <p>Mutlak Müşteri memnuniyeti, deneyimli personel </p>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div> 
                            </div>
                      </div>
                    </div>
     </div>
     </div>
</div>
     <!--news-->
<div class="news-section" id="news">
            <div class="container">
                 <h3 class="tittle">duyurular</h3>
                <div class="news-left">         
                    <div class="col-md-6 col-news-right">
                        <div class="col-news-top">
                            <img class="img-responsive mix-in" src="images/ramazan.jpg" alt="">
                      <div class="clearfix"> </div>
                            <div class="col-bottom">
                        </div>
                        </div>
                        </div>  
<div class="col-news-top">
                            <div class="col-bottom">
                            <h4>TÜRKİŞ ŞUBEMİZ HİZMETİNİZDE</h4>
                        </div>
<a href="#" class="date-in">
                                <img class="img-responsive mix-in" src="images/c1.jpg" alt="">
                                <div class="month-in">
                                  <label><span class="day">29</span>
                                    <span class="month">Aralık</span>
                                  </label>
                                </div>
                            </a>
<div class="clearfix"> </div>
<div class="news-left">         
                    <div class="col-md-6 col-news-right">
                        <div class="col-news-top">
                            <a href="#" class="date-in">
                                <img class="img-responsive mix-in" src="images/c4.jpg" alt="">
                                <div class="month-in">
                                  <label>
                                    <span class="day">11</span>
                                    <span class="month">Mart</span>
                                  </label>
                            </div>
                            </a>
                            <div class="clearfix"> </div>
                            <div class="col-bottom">
                            <h4>DEREBAHÇE ŞUBEMİZ HİZMETİNİZDE</h4>
                        </div>
                        </div>
                        </div>  
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
        </div>
    <!--//news-->
  </div>
</div>
</div>
<!--footer-->
<div class="footer">
<div class="container">
            <div class="footer-top">
                <div class="col-md-4 amet-sed">
                <h4>küçükpazar</h4>
                <ul class="social">
                    <li><i class="glyphicon glyphicon-home"> </i>Gazi Cad. No:82 İlkadım / Samsun</li>
                    <li class="tele-phone"><i class="glyphicon glyphicon-earphone"> </i>(0362) 432 41 03</li>
                    <li class="tele-phone"><i class="glyphicon glyphicon-earphone"> </i>Fax:(0362) 432 08 65</li>
                    <li class="mail"><a href="mailto:info@kucukpazar.com"><i class="glyphicon glyphicon-envelope"> </i> info@kucukpazar.com</a></li>
                </ul>
                </div>
                <div class="col-md-4 amet-sed ">
    <h4>BİZİ TAKİP EDİN</h4>
                        <ul class="social-icons2 wow slideInDown" data-wow-duration="1s" data-wow-delay=".3s">
                                        <li><a href="https://www.facebook.com/kucukpazar" class="fb"> </a></li>
                                        <li><a href="https://instagram.com/kucukpazar" class="in"></a></li>
                                        <li><a href="https://twitter.com/kucukpazar/" class="tw"></a></li>
                                         <div class="clearfix"></div>
                                    </ul>
                                    
              </div>
                <div class="clearfix"> </div>
            </div>
                <p class="footer-class">Copyright © 2017 küçükpazar
  </p>
  </div>
</div>
</div>
</div>
   


@endsection


@section("js")

<script type="text/javascript" src="{{URL::to('')}}/js/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="{{URL::to('')}}/js/move-top.js"></script>
<script type="text/javascript" src="{{URL::to('')}}/js/easing.js"></script>
<script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
                });
            });
</script>
<!-- swipe box js -->
<script type="text/javascript" src="{{URL::to('')}}/js/jquery.leoslider.js"></script>
<script type="text/javascript" src="{{URL::to('')}}/js/main.js"></script>



 <script src="{{ URL :: to('')}}/js/responsiveslides.min.js"></script>
                         <script>
                            // You can also use "$(window).load(function() {"
                            $(function () {
                              // Slideshow 3
                              $("#slider3").responsiveSlides({
                                auto: true,
                                pager:false,
                                nav: true,
                                speed: 500,
                                namespace: "callbacks",
                                before: function () {
                                  $('.events').append("<li>before event fired.</li>");
                                },
                                after: function () {
                                  $('.events').append("<li>after event fired.</li>");
                                }
                              });
                        
                            });
                          </script>
        <!--start-smooth-scrolling-->
                        <script type="text/javascript">
                                    $(document).ready(function() {
                                        /*
                                        var defaults = {
                                            containerID: 'toTop', // fading element id
                                            containerHoverID: 'toTopHover', // fading element hover id
                                            scrollSpeed: 1200,
                                            easingType: 'linear' 
                                        };
                                        */
                                        
                                        $().UItoTop({ easingType: 'easeOutQuart' });
                                        
                                    });
                                </script>
                                <!--end-smooth-scrolling-->
        <a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <script src="{{ URL :: to('')}}/js/bootstrap.js"></script>

@endsection
