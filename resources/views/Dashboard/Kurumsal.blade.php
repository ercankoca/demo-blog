@extends('Dashboard/layouts.app')

@section('content')


@foreach($rows as $row)
	
  

    <div class="panel panel-default">
   <div class="panel-heading">
    <h3 class="panel-title">Kurumsal</h3>
  </div>
  <div class="panel-body">

     <form action="/Dashboard/Kurumsal" method="POST" >
      {{@csrf_field()}}
      <input type="hidden" name="id" value ="{{$row->id}}"/ >

        <div class="form-group">
                  <label> Başlık </label>
                  <textarea class="form-control" name="title" rows="3" value="{{$row->content}}">{{$row->title}}</textarea>
                  </div>


        <div class="form-group">
                  <label> İçerik</label>
                  <textarea class="form-control" name="content" rows="3" value="{{$row->content}}">{{$row->content}}</textarea>
                  </div>

       
   
       <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Güncelle</button>
                    </div>
      </form>
   
     </div>
	


	@endforeach

@endsection