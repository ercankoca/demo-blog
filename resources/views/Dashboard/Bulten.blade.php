@extends('Dashboard/layouts.app')

@section('content')

@foreach($rows as $row)
	
	<div class="col-md-6">

		<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Kampanya Tarihleri</h3>
  </div>
  <div class="panel-body">

  	<form action="/Dashboard/UpdateBulten" method="POST">

  		{{@csrf_field()}}

	 	<div class="form-group">

                  <label>İlk Tarihi Seçiniz</label>
                  <input type="date" name="first_date" class="form-control" value="{{$row->first_date}}"/>
                </div>

         <div class="form-group">
                  <label>Son Tarihi Seçiniz</label>
                  <input type="date" name="last_date" class="form-control" value="{{$row->last_date}}"/>
                </div>

         <div class="form-group">
                  <label>Bülten No</label>
                  <input type="text" name="order" class="form-control" value="{{$row->order}}"/>
                </div>

        <div class="form-group">
            
  
  		  
   
			 <div class="box-footer">
			                <button type="submit" class="btn btn-primary">Güncelle</button>
			              </div>
      </form>
   
     </div>
 </div></div></div>

@endforeach


@endsection