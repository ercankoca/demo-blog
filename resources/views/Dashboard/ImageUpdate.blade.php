@extends('Dashboard/layouts.app')

@section('content')


@foreach($image as $row)
	
	<div class="col-md-6">

		<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Ürün Görseli</h3>
  </div>
  <div class="panel-body">
  	 <form action="/Dashboard/UpdatePageImage" method="POST" >
  	 	{{@csrf_field()}}
  	 	<input type="hidden" name="id" value ="{{$row->id}}"/ >
   <img src="{{ URL:: to('') }}/upload/{{$row->filename}}" width="100%" height="400" />
    </div>
   </div>	    
	
</div>
	<div class="col-md-6">

		<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Ürün Bilgileri</h3>
  </div>
  <div class="panel-body">


	 	<div class="form-group">
                  <label>Ürün Başlık</label>
                  <input type="text" name="filetitle" class="form-control" value="{{$row->filetitle}}"/>
                </div>

        <div class="form-group">
                  <label>Ürün İçerik</label>
                  <textarea class="form-control" name="filecontent" rows="3" value="{{$row->filecontent}}">{{$row->filecontent}}</textarea>
                  </div>

          <div class="form-group">
                  <label>Ürün Yeni Fiyatı</label>
                  <input type="text" name="price" class="form-control" value="{{$row->price}}"/>
                </div> 

            <div class="form-group">
                  <label>Ürün Eski Fiyatı</label>
                  <input type="text" name="price_old" class="form-control" value="{{$row->price_old}}"/>
                </div>         
  
  		  
             <div class="form-group">
                  <label>Resim Alanı</label>
                  <select class="form-control" name="status">
                    @if($row->status=="0")
                     <option value="{{$row->status}}">Resim Alanını Seçiniz</option>
                    @elseif($row->status=="1")
                    <option value="{{$row->status}}">Üst slider</option>
                     @elseif($row->status=="2")
                    <option value="{{$row->status}}">ürün</option>
                     @elseif($row->status=="3")
                    <option value="{{$row->status}}">Orta Slider</option>
                       
                    @endif
                   
                     <option value="1">Üst Slider</option>
                    <option value="2">Ürün</option>
                    <option value="3">Orta Slider</option>
                   
                  </select>
                </div>
   
			
           <div class="box-footer">
                      <button style="float: left" type="submit" class="btn btn-primary">Güncelle</button></form>
                    
   <form action="{{ url('Dashboard/Delete/') }}" method="POST">
                                                {{ csrf_field() }}
                                                 {{ method_field('DELETE') }}
                                                <input type="hidden" name="id" value="{{$row->id}}"/>

                                               <button  style="margin-left:20px" type="submit" id="{{ $row->id }}" class="delete btn btn-danger">

                                                    <i class="fa fa-btn fa-trash"></i>Sil
                                                </button>
                                              
                                            </form>
                               
                   
      
   
        </div> 
       
        
      

     


  

	@endforeach

@endsection