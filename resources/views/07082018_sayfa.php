


@extends('layouts.app')

@section('content')


<!-- Revolution Slider -->
<section class="revolution-slider">
  <div class="bannercontainer">
    <div class="banner">
      <ul>

        @foreach($slider as $row)
        
          <!-- Slide 1 -->
         <li data-transition="slotslide-vertical" data-slotamount="7" data-masterspeed="1500" >
          <!-- Main Image --> 
          <img src="{{ URL::to('') }}/upload/{{$row->filename}}" style="opacity:0;" alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
         </li>

        @endforeach

      

      </ul>
    </div>
  </div>
</section>
<!-- Rooms -->
<section class="rooms mt50">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">

<div class="alert alert-success" role="alert">
  




              @foreach($bultens as $row) 

                

              <h3><b>{{$row->order}}</b> Sayılı Bülten <br> <b>  {{date('d F Y ', strtotime($row->first_date))}}   -
               {{date('d F Y ', strtotime($row->last_date))}}</b> Tarihleri Arasında Geçerlidir.</h3></div>

                 @endforeach
              
     
     
     @foreach($product as $row)

      <div class="col-sm-4 col-lg-3 getir" data-toggle="modal" data-target="#myModal" id="{{ $row->id }}" style=" cursor: pointer;" >
        <div class="room-thumb"> <img src="{{ URL::to('') }}/upload/{{$row->filename}}" alt="ürün " class="background-size: cover; img-responsive" />
          <div class="mask" style="width: 100% ">
            <div class="main main2" style="">
              <h5 class="urunad">{{$row->filetitle}} </h5>
              <div class="price" style=" ">  {{$row->price}}  TL</div>
            </div>
          
          </div>
        </div>
      </div>
      @endforeach

      <!-- product -->

</section>







 <div class="container" style="padding-top: 8%">
  
    <!--//screen-gallery-->
            <div class="sreen-gallery-cursual">
               <!-- required-js-files-->
          
                 <!--//required-js-files-->
                   <div id="owl-demo" class="owl-carousel">

                    @foreach($ortaslider as $row)

                    <div class="item-owl ">
                              <div class="test-review">
                                <img src="images/s5.jpg" class="img-responsive" alt=""/>
                        <h5>Plastik Mutfak Gereçleri</h5>
                                </div>
                         </div>
                
                    @endforeach

                      </div>
            <!--//screen-gallery-->

</div>

</div>


<section class="rooms mt50">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">

              
     
     
     @foreach($product as $row)

      <div class="col-sm-4 col-lg-3 getir" data-toggle="modal" data-target="#myModal" id="{{ $row->id }}" style=" cursor: pointer;" >
        <div class="room-thumb"> <img src="{{ URL::to('') }}/upload/{{$row->filename}}" alt="ürün " class="background-size: cover; img-responsive" />
          <div class="mask" style="width: 100% ">
            <div class="main main2" style="">
              <h5 class="urunad">{{$row->filetitle}} </h5>
              <div class="price" style=" ">  {{$row->price}}  TL</div>
            </div>
            <div class="content">
              <p>{{$row->filecontent}}</p>
            
<a class="btn btn-primary btn-block" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fweecomi.com/{{ URL::to('') }}/upload/{{$row->filename}}" >

  Facebookta paylaş

</a>    

 
            </div>
          </div>
        </div>
      </div>
      @endforeach

      <!-- product -->

</section>


<!-- Page Content -->
<div class="container">

  <div class="row">

    <div class="col-lg-12">
      <h1 class="page-header">Thumbnail Gallery</h1>
    </div>
@foreach($product as $row)
    <div class="col-lg-3 col-md-4 col-xs-4 thumb" data-toggle="modal" data-target="#myModal" id="{{ $row->id }}" style=" cursor: pointer;" >
      <a class="thumbnail" >
        <img class="img-responsive" src="{{ URL::to('') }}/upload/{{$row->filename}}" alt="">
      </a>
       <div class="" style="">
              <h5 class="content">{{$row->filetitle}} <span class="" style="float:right;">{{$row->price}}  TL</span> </h5>
             
            </div>
    </div>
   
@endforeach
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="row">
      <div class="col-lg-12">
        <p>Copyright &copy; Your Website 2014</p>
      </div>
    </div>
  </footer>

</div>
<!-- /.container -->


<!-- USP's -->



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="top: 10%">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>
     
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
      </div>
    </div>

  </div>
</div>







@endsection

@section("js")

<script>

$(".getir").click(function(){

  var index = $(".getir").index(this);

  var id = $(".getir").eq(index).attr("id");

  var urunad = $(".urunad").eq(index).html();

  $(".modal-title").html(urunad);

  $.ajax({
    url:"{{ URL::to("") }}/verilerigetir/"+id,
    type:"GET",
    success:function(r){
      $(".modal-body").html(r);
    }
  });


});



  </script>



      <link href="css/owl.carousel.css" rel="stylesheet">
                  <script src="js/owl.carousel.js"></script>
                      <script>
                  $(document).ready(function() {
                    $("#owl-demo").owlCarousel({
                      items :5,
                      lazyLoad : true,
                      autoPlay : true,
                      navigation :true,
                      navigationText :  false,
                      pagination : true,
                    });
                  });
                  </script>

@endsection