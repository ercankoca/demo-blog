<!DOCTYPE HTML>
<html>

<!-- Mirrored from kucukpazar.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Jul 2018 16:36:23 GMT -->
<head>
<title>küçükpazar Alışveriş Merkezi</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="İhtiyacınız olan ne varsa hepsi küçükpazar'da!
Ev gereçleri, plastik ev eşyaları, mutfak gereçleri, züccaciye ürünleri, banyo aksesuarları, hırdavat, hediyelik eşya, kırtasiye, kozmetik, temizlik, oyuncak, elektrikli ev aletleri en uygun fiyatlarla küçükpazar mağazağalarında sizleri bekliyor.">
<meta name="keywords" content="küçükpazar,samsun alışveriş merkezi, çorum alışveriş merkezi ">
<meta name="author" content="skoc">
<meta name="copyright" content= "2017 kucukpazar.com">
<meta name="abstract" content="samsun alışveriş merkezi, çorum alışveriş merkezi, ">
<meta name="robots" content="index,follow"> 
<meta name="googlebot" content="Index, Follow">
<meta name="rating" content="all">
<meta name="revisit-after" content="1 days">
<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/iconeffects.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />  
<link rel="stylesheet" href="css/leoslider.css">
<link href="css/iconeffects.css" rel='stylesheet' type='text/css' />
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Nosifer' rel='stylesheet' type='text/css'>
<!--/script-->
<script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
                });
            });
</script>
<!-- swipe box js -->
    <script src="js/jquery.leoslider.js"></script> 
        <script src="js/main.js"></script> 
<!-- //swipe box js -->


</head>
<body>
<!--start-home-->
<div class="main-header" id="house">
            <div class="header-strip">
               <div class="container">
                <p class="envelope"><a href="mailto:info@kucukpazar.com">
          <span class="glyphicon glyphicon-envelope"  aria-hidden="true"></span> info@kucukpazar.com</a></p>
                <p class="phonenum"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>0362 432 41 03</p>
                <div class="social-icons">
                    <ul>
                        <li><a href="https://www.facebook.com/kucukpazar"><i class="facebook"> </i></a></li>
                        <li><a href="https://instagram.com/kucukpazar"><i class="dribble"></i> </a></li>    
                        <li><a href="https://twitter.com/kucukpazar"><i class="twitter"></i></a></li>                                   
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
            <div class="header-top">
            <div class="logo"><a href="index-2.html"><h1><span><img src="images/kucukpazar_logo.png"></span></h1></a></div>
            <nav class="navbar navbar-default">
      <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                            </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="float: left">
              <ul class="nav navbar-nav">
                <li><a class="active" href="index-2.html">Anasayfa<span class="sr-only">(current)</span></a></li>
                <li><a href="kurumsal.html">Kurumsal</a></li>
                <li><a href="magazalar.html">Mağazalar</a></li>
                <li><a href="kariyer.html">Kariyer</a></li>
                <li><a href="kampanyalar.html">Kampanyalar</a></li>
                <li><a href="iletisim.html">İletişim</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
            <div class="clearfix"></div>
    </div>


     <section id="line-slider">
            <div class="leoslider">
              <ul class="slides">
               <li>
              <a href="kampanyalar.html"><img src="images/1.jpg" alt="Haziran Ayı İndirim Kataloğu " /></a></li>
                                               <li>
                   <img src="images/ramazan1.jpg" alt=" " />
                </li>
                <li>
                 <img src="images/ramazan2.jpg" alt=" " />
                </li>
                <li>
                 <img src="images/7.jpg" alt=" " />
                </li>
                <li>
               <img src="images/3.jpg" alt=" " />
                </li>
                <li>
                 <img src="images/4.jpg" alt=" " />
                </li>
                <li>
                 <img src="images/8.jpg" alt=" " />
              </ul>
            </div>
        </section>
        <!-- /Line Slider -->
</div>
<div class="banner-bottom">
                <!--//screen-gallery-->
                        <div class="sreen-gallery-cursual">
                             <!-- required-js-files-->
                            <link href="css/owl.carousel.css" rel="stylesheet">
                                <script src="js/owl.carousel.js"></script>
                                    <script>
                                $(document).ready(function() {
                                  $("#owl-demo").owlCarousel({
                                    items :5,
                                    lazyLoad : true,
                                    autoPlay : true,
                                    navigation :true,
                                    navigationText :  false,
                                    pagination : true,
                                  });
                                });
                                </script>
                                 <!--//required-js-files-->
                               <div id="owl-demo" class="owl-carousel">
                                  <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s5.jpg" class="img-responsive" alt=""/>
                                              <h5>Plastik Mutfak Gereçleri</h5>
                                            </div>
                                 </div>
                                     <div class="item-owl">
                                        <div class="test-review">
                                              <img src="images/s6.jpg" class="img-responsive" alt=""/>
                                              <h5>Züccaciye</h5>
                                       </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s2.jpg" class="img-responsive" alt=""/>
                                              <h5>Banyo Aksesuarları</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s1.jpg" class="img-responsive" alt=""/>
                                              <h5>Hediyelik Eşya</h5>
                                            </div>
                                    </div>
                                    <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s4.jpg" class="img-responsive" alt=""/>
                                              <h5>Balkon&amp;Bahçe</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s3.jpg" class="img-responsive" alt=""/>
                                              <h5>Hırdavat</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s7.jpg" class="img-responsive" alt=""/>
                                              <h5>Plastik Banyo Gereçleri</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                        <div class="test-review">
                                              <img src="images/s8.jpg" class="img-responsive" alt=""/>
                                            <h5>Ev Elektroniği</h5>
                                       </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s9.jpg" class="img-responsive" alt=""/>
                                              <h5>Kozmetik</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s10.jpg" class="img-responsive" alt=""/>
                                              <h5>Kırtasiye</h5>
                                            </div>
                                    </div>
                                     <div class="item-owl">
                                            <div class="test-review">
                                              <img src="images/s11.jpg" class="img-responsive" alt=""/>
                                              <h5>Oyuncak</h5>
                                            </div>
                                            
                                    </div>
                              </div>
                        <!--//screen-gallery-->

          </div>
 </div>

<!--news-->

<div class="projects">
  <div class="container">
    <h3 class="tittle">İHTİYACINIZ OLAN NE VARSA <span>KÜÇÜKPAZAR'DA!</span></h3>
      <div class="projects-inner">
        <div class="col-md-7 banner-slider">
                            <div class="callbacks_container">
                                <ul class="rslides" id="slider3">
                                    <li>
                                      <div class="blog-img">
                                         <img src="images/pro1.jpg" class="img-responsive" alt="" />
                                      </div>
                                
                                    </li>
                                    <li>
                                      <div class="blog-img">
                                        <img src="images/pro2.jpg" class="img-responsive" alt="" />
                                      </div>
                                
                                  </li>
                                  
                                </ul>
                          </div>
        </div>
                    <div class="col-md-5 ban-text">
                       <div class="choose">
                            <div class="choose_img">
                                 <h3>Neden Biz ?</h3>
                                   <!-- choose icon -->
                                   <div class="choose_icon">
                                        <div class="choose_left">
                                            <span class="glyphicon glyphicon-home"></span>
                                        </div>
                                            <div class="choose_right">
                                                <p>Zengin ürün çeşidi,</p>
                                            </div>
                                        <div class="clearfix"></div>
                              </div>
                                 
                                  <!-- choose icon -->
                                  <div class="choose_icon">
                                    <div class="choose_left">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </div>
                                    <div class="choose_right">
                                        <p>En yeni ürünler en uygun fiyatlarla</p>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div>
                                 
                                  <!-- choose icon -->
                                  <div class="choose_icon">
                                    <div class="choose_left">
                                        <span class="glyphicon glyphicon-heart-empty"></span>
                                    </div>
                                    <div class="choose_right">
                                        <p>Mutlak Müşteri memnuniyeti, deneyimli personel </p>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div> 
                            </div>
                      </div>
                    </div>
     </div>
     </div>
</div>
     <!--news-->
<div class="news-section" id="news">
            <div class="container">
                 <h3 class="tittle">duyurular</h3>
                <div class="news-left">         
                    <div class="col-md-6 col-news-right">
                        <div class="col-news-top">
                            <img class="img-responsive mix-in" src="images/ramazan.jpg" alt="">
                      <div class="clearfix"> </div>
                            <div class="col-bottom">
                        </div>
                        </div>
                        </div>  
<div class="col-news-top">
                            <div class="col-bottom">
                            <h4>TÜRKİŞ ŞUBEMİZ HİZMETİNİZDE</h4>
                        </div>
<a href="#" class="date-in">
                                <img class="img-responsive mix-in" src="images/c1.jpg" alt="">
                                <div class="month-in">
                                  <label><span class="day">29</span>
                                    <span class="month">Aralık</span>
                                  </label>
                                </div>
                            </a>
<div class="clearfix"> </div>
<div class="news-left">         
                    <div class="col-md-6 col-news-right">
                        <div class="col-news-top">
                            <a href="#" class="date-in">
                                <img class="img-responsive mix-in" src="images/c4.jpg" alt="">
                                <div class="month-in">
                                  <label>
                                    <span class="day">11</span>
                                    <span class="month">Mart</span>
                                  </label>
                            </div>
                            </a>
                            <div class="clearfix"> </div>
                            <div class="col-bottom">
                            <h4>DEREBAHÇE ŞUBEMİZ HİZMETİNİZDE</h4>
                        </div>
                        </div>
                        </div>  
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
        </div>
    <!--//news-->
  </div>
</div>
</div>
<!--footer-->
<div class="footer">
<div class="container">
            <div class="footer-top">
                <div class="col-md-4 amet-sed">
                <h4>küçükpazar</h4>
                <ul class="social">
                    <li><i class="glyphicon glyphicon-home"> </i>Gazi Cad. No:82 İlkadım / Samsun</li>
                    <li class="tele-phone"><i class="glyphicon glyphicon-earphone"> </i>(0362) 432 41 03</li>
                    <li class="tele-phone"><i class="glyphicon glyphicon-earphone"> </i>Fax:(0362) 432 08 65</li>
                    <li class="mail"><a href="mailto:info@kucukpazar.com"><i class="glyphicon glyphicon-envelope"> </i> info@kucukpazar.com</a></li>
                </ul>
                </div>
                <div class="col-md-4 amet-sed ">
    <h4>BİZİ TAKİP EDİN</h4>
                        <ul class="social-icons2 wow slideInDown" data-wow-duration="1s" data-wow-delay=".3s">
                                        <li><a href="https://www.facebook.com/kucukpazar" class="fb"> </a></li>
                                        <li><a href="https://instagram.com/kucukpazar" class="in"></a></li>
                                        <li><a href="https://twitter.com/kucukpazar/" class="tw"></a></li>
                                         <div class="clearfix"></div>
                                    </ul>
                                    
              </div>
                <div class="clearfix"> </div>
            </div>
                <p class="footer-class">Copyright © 2017 küçükpazar
  </p>
  </div>
</div>
</div>
</div>
    <script src="js/responsiveslides.min.js"></script>
                         <script>
                            // You can also use "$(window).load(function() {"
                            $(function () {
                              // Slideshow 3
                              $("#slider3").responsiveSlides({
                                auto: true,
                                pager:false,
                                nav: true,
                                speed: 500,
                                namespace: "callbacks",
                                before: function () {
                                  $('.events').append("<li>before event fired.</li>");
                                },
                                after: function () {
                                  $('.events').append("<li>after event fired.</li>");
                                }
                              });
                        
                            });
                          </script>
        <!--start-smooth-scrolling-->
                        <script type="text/javascript">
                                    $(document).ready(function() {
                                        /*
                                        var defaults = {
                                            containerID: 'toTop', // fading element id
                                            containerHoverID: 'toTopHover', // fading element hover id
                                            scrollSpeed: 1200,
                                            easingType: 'linear' 
                                        };
                                        */
                                        
                                        $().UItoTop({ easingType: 'easeOutQuart' });
                                        
                                    });
                                </script>
                                <!--end-smooth-scrolling-->
        <a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <script src="js/bootstrap.js"></script>
</body>

<!-- Mirrored from kucukpazar.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Jul 2018 16:40:25 GMT -->
</html>