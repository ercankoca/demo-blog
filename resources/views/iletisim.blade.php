 @extends('layouts.app')

@section('content')

<div class="container">
  <div class="row"> 
    
    <!-- Contact details -->
    <section class="contact-details">
      <div class="col-md-5">
        <h2 class="lined-heading  mt50"><span>Address</span></h2>
        <!-- Panel -->
        <div class="panel panel-default text-center">
          <div class="panel-heading">
            <div class="panel-title"><i class="fa fa-star"></i> <strong>İletişim Bilgileri</strong></div>
          </div>
          <div class="panel-body">


              <address>
                @foreach($iletisim as $row)
                {{$row->adres}}
                  <br>
                  <abbr title="Phone">P:</abbr> <a href="#">{{$row->telefon}} </a><br>
                  <abbr title="Email">E:</abbr> <a href="#">{{$row->mail}}</a><br>
                  <abbr title="Website">W:</abbr> <a href="#">{{$row->website}} </a><br>
              
                  @endforeach
              </address>


         
            <p><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12167.46875579907!2d36.5165236!3d40.323105!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8928f1d4982ab73f!2s%C4%B0stanbul+Evim+Avm!5e0!3m2!1str!2str!4v1533821111825" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></p>
        
   
          </div>


        </div>
        <!-- GMap -->
 
      </div>
    </section>
    
    <!-- Contact form -->
    <section id="contact-form" class="mt50">
      <div class="col-md-7">
        <h2 class="lined-heading"><span>Send a message</span></h2>
        <p>Pellentesque facilisis justo sed enim facilisis luctus. Duis pretium nibh at lectus tempus, vel lacinia quam adipiscing. Nullam luctus congue mattis.</p>
        <form class="clearfix mt50" role="form" >
          
            {{@csrf_field()}}
          <!-- Error message -->
      <div id="message"></div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="name" accesskey="U"><span class="required">*</span> Your Name</label>
                <input name="name" type="text" id="name" class="form-control" value=""/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="email" accesskey="E"><span class="required">*</span> E-mail</label>
                <input name="email" type="text" id="email" value="" class="form-control"/>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="subject" accesskey="S">Subject</label>
            <select name="subject" id="subject" class="form-control">
              <option value="Booking">Seç1</option>
              <option value="a Room">Seç2</option>
              <option value="other">Seç3</option>
            </select>
          </div>
          <div class="form-group">
            <label for="comments" accesskey="C"><span class="required">*</span> Your message</label>
            <textarea name="comments" rows="9" id="comments" class="form-control"></textarea>
          </div>
          <div class="form-group">
            <label><span class="required">*</span> Spam Filter: &nbsp;&nbsp;&nbsp;1 + 1 =</label>     
            <input name="verify" type="text" id="verify" value="" class="form-control" placeholder="Please enter the outcome" />
          </div>
          <button type="submit" class="btn  btn-lg btn-primary">Send message</button>
        </form>
      </div>
    </section>
  </div>
</div>



@endsection  
  