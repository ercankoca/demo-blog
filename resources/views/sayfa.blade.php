


@extends('layouts.app')

@section('content')


<!-- Revolution Slider -->
<section class="revolution-slider">
  <div class="bannercontainer">
    <div class="banner">
      <ul>

        @foreach($slider as $row)
        
          <!-- Slide 1 -->
         <li data-transition="slotslide-vertical" data-slotamount="7" data-masterspeed="1500" >
          <!-- Main Image --> 
          <img src="{{ URL::to('') }}/upload/{{$row->filename}}" style="opacity:0;" alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
         </li>

        @endforeach

      

      </ul>
    </div>
  </div>
</section>
<!-- Rooms -->

<div class="alert alert-red container-fluid" role="alert" style="border-radius: 0px ;text-align:center;  background-color: red;margin-top:-13px; margin-bottom: -37px; padding:0px">
   @foreach($bultens as $row)

              <h3 style=" color:#FFFFFF; padding: -40px ; font-size:11px"><b>{{$row->order}}</b> Sayılı Bülten  <b>   <?php echo iconv('latin5','utf-8',strftime(' %d  ',strtotime($row->first_date))); ?>   -
                <?php echo iconv('latin5','utf-8',strftime(' %d %B ',strtotime($row->last_date))); ?></b> Tarihleri Arasında Geçerlidir.</h3></div>


                 @endforeach



<section class="rooms mt50">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">

              
     
     
    <div class="row">

<?php $lastid= 0;?>

@foreach($product as $row)
    <div class="col-lg-3 col-md-4 col-xs-4 thumb getir" data-toggle="modal" data-target="#myModal" id="{{ $row->id }}" 
      style=" cursor: pointer;" >
      <a class="thumbnail " style="display: inline-block" >
        <img class="img-responsive" src="{{ URL::to('') }}/upload/{{$row->filename}}" width="100%" height="300px" alt="">  
             
              </a>
<dl class="row">
  <dd class="col-sm-8 col-lg-8">  {{$row->filetitle}}</dd>
  <dt class="col-sm-4 col-lg-4"> <span class="label label-default" style="" > {{$row->price}}  TL </span>
              <span class="label label-default" style="text-decoration:line-through;"> {{$row->price_old}}  TL </span></dt>
  </dl>
    
        
    </div>
  <?php $lastid=$row->id;?>
@endforeach
  </div>

  <hr>

      <!-- product -->

</section>







 <section class="rooms mt50">
  <div class="container">
  
    <!--//screen-gallery-->
            <div class="sreen-gallery-cursual">
               <!-- required-js-files-->
          
                 <!--//required-js-files-->
                   <div id="owl-demo" class="owl-carousel" >

                    @foreach($ortaslider as $row)

                    
         
     

                    <div class="item-owl " >
                              <div class="test-review" style="margin: 10px">
                                <img src="{{ URL::to('') }}/upload/{{$row->filename}}" class="img-responsive" alt=""/>
                        <h5>{{$row->filecontent}}</h5>
                                </div>
                         </div>
               
                    @endforeach

                      </div>
            <!--//screen-gallery-->

</div>
<hr>
    <div class="row">

@foreach($product2 as $row)
@if($row->id<$lastid)
    <div class="col-lg-3 col-md-4 col-xs-4 thumb getir" data-toggle="modal" data-target="#myModal" id="{{ $row->id }}" style=" cursor: pointer;" >
      <a class="thumbnail" >
        <img class="img-responsive" src="{{ URL::to('') }}/upload/{{$row->filename}}" alt="">
      </a>
     <dl class="row">
  <dd class="col-sm-8 col-lg-8">  {{$row->filetitle}}</dd>
  <dt class="col-sm-4 col-lg-4"> <span class="label label-default" style="" > {{$row->price}}  TL </span>
              <span class="label label-default" style="text-decoration:line-through;"> {{$row->price_old}}  TL </span></dt>
  </dl>


    </div>
    @endif
@endforeach
  </div>

  <hr>

</div>
</section>






<!-- USP's -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="top: 10%">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p>
     
        </p>
      </div>
      <div class="modal-footer">
    

      

        <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
      </div>
    </div>

  </div>
</div>







@endsection

@section("js")

<script>

$(".getir").click(function(){

  var index = $(".getir").index(this);

  var id = $(".getir").eq(index).attr("id");

  var urunad = $(".urunad").eq(index).html();

  $(".modal-title").html(urunad);

  $.ajax({
    url:"{{ URL::to("") }}/verilerigetir/"+id,
    type:"GET",
    success:function(r){
      $(".modal-body").html(r);
    }
  });


});



  </script>



      <link href="css/owl.carousel.css" rel="stylesheet">
                  <script src="js/owl.carousel.js"></script>
                      <script>
                  $(document).ready(function() {
                    $("#owl-demo").owlCarousel({
                      items :5,
                      lazyLoad : true,
                      autoPlay : true,
                      navigation :true,
                      navigationText :  false,
                      pagination : true,
                    });
                  });



    function fbShare(url, title, descr, image, winWidth, winHeight) {
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open('https://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    }




                  </script>

@endsection