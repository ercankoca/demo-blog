<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

<title>küçükpazar Alışveriş Merkezi</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="İhtiyacınız olan ne varsa hepsi küçükpazar'da!
Ev gereçleri, plastik ev eşyaları, mutfak gereçleri, züccaciye ürünleri, banyo aksesuarları, hırdavat, hediyelik eşya, kırtasiye, kozmetik, temizlik, oyuncak, elektrikli ev aletleri en uygun fiyatlarla küçükpazar mağazağalarında sizleri bekliyor.">
<meta name="keywords" content="küçükpazar,samsun alışveriş merkezi, çorum alışveriş merkezi ">
<meta name="author" content="skoc">
<meta name="copyright" content= "2017 kucukpazar.com">
<meta name="abstract" content="samsun alışveriş merkezi, çorum alışveriş merkezi, ">
<meta name="robots" content="index,follow"> 
<meta name="googlebot" content="Index, Follow">
<meta name="rating" content="all">
<meta name="revisit-after" content="1 days">
<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/iconeffects.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />  
<link rel="stylesheet" href="css/leoslider.css">
<link href="css/iconeffects.css" rel='stylesheet' type='text/css' />
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Nosifer' rel='stylesheet' type='text/css'>
<!--/script-->
<script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
                });
            });
</script>
<!-- swipe box js -->
    <script src="js/jquery.leoslider.js"></script> 
        <script src="js/main.js"></script> 
<!-- //swipe box js -->

</head>
<body>
    
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
               
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    
                  

                    <ul class="navbar-nav mr-auto">

                         <li class="nav-item">
                                <a class="nav-link" href="{{ route('anasayfa') }}">Ansayfa</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('kurumsal') }}">Kurumsal</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('magazalar') }}">Mağazalar</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('kariyer') }}">Kariyer</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('kampanyalar') }}">Kampanyalar</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('iletisim') }}">İletişim</a>
                            </li>

                    </ul>

                    <!-- Right Side Of Navbar -->
           
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
