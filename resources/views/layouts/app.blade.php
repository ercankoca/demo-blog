<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>İstanbul Ucuzluk Pazarı</title>
<meta property="og:title" content="İstanbul Ucuzluk Pazarı"  />
<meta property="og:description" content="Uygun Fiyat, Geniş Ürün Yelpazesi ve Taksit İmkanlarıyla. %100 Güvenli Alışveriş." />
<meta property="og:image" content="Uygun Fiyat, Geniş Ürün Yelpazesi ve Taksit İmkanlarıyla. %100 Güvenli Alışveriş." />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" href="favicon.ico">

<!-- Stylesheets -->
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/animate.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/bootstrap.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/owl.carousel.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/owl.theme.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/prettyPhoto.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/smoothness/jquery-ui-1.10.4.custom.min.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/rs-plugin/css/settings.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/theme.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/colors/blue.css">
<link rel="stylesheet" href="{{ URL::to('') }}/assets/css/responsive.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600,700">



<!-- Javascripts --> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery-1.11.0.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/bootstrap-hover-dropdown.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery.nicescroll.js"></script>  
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery.prettyPhoto.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery-ui-1.10.4.custom.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery.forms.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery.sticky.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/waypoints.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery.isotope.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/jquery.gmap.min.js"></script> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{{ URL::to('') }}/assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
<script type="text/javascript" src="{{ URL::to('') }}/assets/js/custom.js"></script> 



<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Top header -->
<div id="top-header" >
  <div class="container" style="border-color: red" >
    <div class="row" >
      <div class="col-xs-6">
        <div class="th-text pull-left">
          <div class="th-item" >
            <div class="th-items" >
     <!-- <a  style="background-color: red; color:white" href="https://wa.me/905418745963"><img src="images/WhatsApp_Icon.png" style="width: 60px; height: 30px; "></a>-->
          </div>
          </div>
          <div class="th-item">                          </div>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="th-text pull-right">
            <div class="th-items" style="color:white">
      <a href="tel://+903562320724"  style="color:white; cursor: pointer"  > +90 356 232 07 24</a>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Header -->
<header>
  <!-- Navigation -->
  <div class="navbar yamm navbar-default" id="sticky">
    <div class="container">
      <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a href="{{ URL::to('anasayfa') }}" class="navbar-brand">         
        <!-- Logo -->
        <div id="logo"> <img id="default-logo" src="{{ URL::to('') }}/images/istanbulucuzluk-logo.png" alt="weecomi" style="height:44px;"> <img id="retina-logo" src="{{ URL::to('') }}/images/istanbulucuzluk-logo.png" alt="weecomi" style="height:44px;"> </div>
        </a> </div>
      <div id="navbar-collapse-grid" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown active"> <a href="{{ URL::to('anasayfa') }}">Anasayfa</a>
          </li>
            <li class="dropdown active"> <a href="{{ URL::to('kurumsal') }}">Kurumsal</a>
          </li>
            <li class="dropdown active"> <a href="{{ URL::to('magazalar') }}">Mağazalar</a>
          </li>
           
           
            <li class="dropdown active"> <a href="{{ URL::to('iletisim') }}">İletişim</a>
          </li>

  
        </ul>
      </div>
    </div>
  </div>
</header>


@yield('content')


<!-- Footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <h4>About demo</h4>
        <p>Suspendisse sed sollicitudin nisl, at dignissim libero. Sed porta tincidunt ipsum, vel volutpat. <br>
          <br>
          Nunc ut fringilla urna. Cras vel adipiscing ipsum. Integer dignissim nisl eu lacus interdum facilisis. Aliquam erat volutpat. Nulla semper vitae felis vitae dapibus. </p>
      </div>
      <!--<div class="col-md-3 col-sm-3">
        <h4>Recieve our newsletter</h4>
        <p>Suspendisse sed sollicitudin nisl, at dignissim libero. Sed porta tincidunt ipsum, vel volutpa!</p>
        <form role="form">
          <div class="form-group">
            <input name="newsletter" type="text" id="newsletter" value="" class="form-control" placeholder="Please enter your E-mailaddress">
          </div>
          <button type="submit" class="btn btn-lg btn-black btn-block">Submit</button>
        </form>
      </div>-->

      <div class="col-md-3 col-sm-3">
        <div class="footer-whatsapp-fix" > <a href="https://api.whatsapp.com/send?phone=905418745963&amp;text=Bilgi%20Almak%20İstiyorum"  title="WhatsApp Sipariş"><i class="fa fa-whatsapp" aria-hidden="true" style="cursor: pointer;"><img 
         style="width: 125px; height: 90px;  margin-left: 210px " src="images/WhatsApp_Icon.png"></i></a></div>

<style>

.footer-whatsapp-fix{display:none;}

@media screen and (max-width: 767px) {

 .footer-whatsapp-fix{display:block;position:fixed; left:100% ;bottom:0;width:0%;line-height:50px;text-align:center;z-index:99999;}

 .footer-whatsapp-fix a{color:#fff;font-size:16px;display:block;}

 .footer-whatsapp-fix a i{color:#fff;font-size:19px;margin-right:5px ; }
 
 }

 @media screen and (min-width: 767px) {

 .footer-whatsapp-fix{display:block;position:fixed;bottom:0;width:100%;line-height:50px;text-align:center;z-index:99999;}

 .footer-whatsapp-fix a{color:#fff;font-size:16px;display:block;}

 .footer-whatsapp-fix a i{color:#fff;font-size:19px;margin-right:5px ; }
 
 }

</style>



        <h4>Address</h4>
        <address>
        <strong>demo</strong><br>

       

        Hocapasa Mahallesi Ebussuud Caddesi<br>
                  No: 19, Sirkecidd <br>
        <abbr title="Phone">P:</abbr> <a href="#"> 032232 2323 23 232 </a><br>
        <abbr title="Email">E:</abbr> <a href="#"> test@gmail.comff </a><br>
        <abbr title="Website">W:</abbr> <a href="#"> www.test.com  </a><br>


        </address>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-6"> &copy; 2018 demo All Rights Reserved </div>

        <div class="col-xs-6 text-right">
          <ul>
            <li><a href="/iletisim">Contact</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>

<!-- Go-top Button -->
<div id="go-top"><i class="fa fa-angle-up fa-2x"></i></div>
@yield('js')
<script type="text/javascript">
  
</script>
</body>
</html>