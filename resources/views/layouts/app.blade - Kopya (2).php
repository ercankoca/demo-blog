<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{URL::to('')}}/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="{{URL::to('')}}/css/iconeffects.css" rel='stylesheet' type='text/css' />
<link href="{{URL::to('')}}/css/style.css" rel='stylesheet' type='text/css' />  
<link rel="stylesheet" href="{{URL::to('')}}/css/leoslider.css">
<link href="css/iconeffects.css" rel='stylesheet' type='text/css' />
/{{URL::to('')}}
<script type="text/javascript" src="{{URL::to('')}}/js/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="{{URL::to('')}}/js/move-top.js"></script>
<script type="text/javascript" src="{{URL::to('')}}/js/easing.js"></script>

<!--/web-font-->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Nosifer' rel='stylesheet' type='text/css'>
<!--/script-->
<script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
                });
            });
</script>
<!-- swipe box js -->
<script type="text/javascript" src="{{URL::to('')}}/js/jquery.leoslider.js"></script>
<script type="text/javascript" src="{{URL::to('')}}/js/main.js"></script>
    
<!-- //swipe box js -->
</head>
<body>
    
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
               
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    
                  

                    <ul class="navbar-nav mr-auto">

                         <li class="nav-item">
                                <a class="nav-link" href="{{ route('anasayfa') }}">Ansayfa</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('kurumsal') }}">Kurumsal</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('magazalar') }}">Mağazalar</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('kariyer') }}">Kariyer</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('kampanyalar') }}">Kampanyalar</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ route('iletisim') }}">İletişim</a>
                            </li>

                    </ul>

                    <!-- Right Side Of Navbar -->
           
                </div>
            </div>
        </nav>

        
            @yield('content')
        
    </div>
</body>
</html>
