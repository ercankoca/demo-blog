@extends('layouts.app')

@section('content')


<div class="container">

      <!-- Page Heading -->
      <h1 class="my-4">Mağazalarımız
       
      </h1>

      <div class="row">


       @foreach($magazalar as $row)
        <div class="col-lg-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="/upload/magazalar/{{$row->image}}" width="100%" height="100%" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">{{$row->title}}</a>
              </h4>
              <p class="card-text">
                {{$row->content}}
              </p>
            </div>
          </div>
        </div>
        @endforeach



      </div>
      <!-- /.row -->



@endsection