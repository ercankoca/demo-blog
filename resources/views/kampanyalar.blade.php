@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">

      <!-- product -->
     <div class="col-lg-12 ">
         
         	<div class="panel panel-default" style="margin-top: 10px">
  <div class="panel-heading">
    <h1 class="panel-title"><h1> 18 Temmuz - 27 Temmuz 2018 </h1> </h1><h5>Tarihleri Arası Tüm Şubelerimizde Geçerlidir
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SAYI :</b> 118
    </h5>
  </div>
 
</div>
        </div>

     
     @foreach($product as $row)

      <div class="col-sm-4 col-md-4 col-lg-4">
        <div class="room-thumb"> <img src="{{ URL::to('') }}/upload/{{$row->filename}}" alt="ürün " class="img-responsive" />
          <div class="mask">
            <div class="main">
              <h5>{{$row->filetitle}}</h5>
              <div class="price">Yeni Fiyat : &euro; {{$row->price}}<br>
              	Eski Fiyat : &euro; {{$row->price_old}}</div>
            </div>
            <div class="content">
              <p>{{$row->filecontent}}</p>
            
<a class="btn btn-primary btn-block" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fweecomi.com/{{ URL::to('') }}/upload/{{$row->filename}}" target="_blank">

  Facebookta paylaş

</a>    

 
            </div>
          </div>
        </div>


      </div>
      @endforeach

      <!-- product -->

  </div>

</div>



@endsection