<?php



 // Authentication Routes...
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        // Registration Routes...
        Route::post('register', 'Auth\RegisterController@register');

        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');





Route::get("/","Frontend\HomeController@index");

Route::get('/anasayfa', 'Frontend\HomeController@index')->name('sayfa');
Route::get('/kurumsal', 'Frontend\HomeController@kurumsal')->name('kurumsal');
Route::get('/magazalar', 'Frontend\HomeController@magazalar')->name('magazalar');
Route::get('/kampanyalar', 'Frontend\HomeController@kampanyalar')->name('kampanyalar');
Route::get('/iletisim', 'Frontend\HomeController@iletisim')->name('iletisim');
Route::get('/verilerigetir/{id}', 'Frontend\HomeController@verileri_getir');
Route::post('/contactForm', 'Frontend\HomeController@contactForm');



Route::group(['prefix' => 'Dashboard',  'middleware' => 'auth'], function(){
	//Route::get('/Dashboard', 'HomeController@index')->name('Dashboard/Dashboard');
Route::get("/","Dashboard\HomeController@index");
Route::get("Upload","Dashboard\ImageUploadController@index");
Route::get("ImageUpdate","Dashboard\ImageUpdateController@index");
Route::get("ImageView","Dashboard\ImageViewController@index");
Route::get("UpdatePage","Dashboard\ImageViewController@UpdatePage");
Route::post("UpdatePage","Dashboard\ImageViewController@UpdatePage");
Route::post("UpdatePageImage","Dashboard\ImageViewController@Update");  
Route::get("Bulten","Dashboard\BultenController@index");
Route::post("UpdateBulten","Dashboard\BultenController@update");


Route::get("Kurumsal","Dashboard\KurumsalController@index");
Route::post("Kurumsal","Dashboard\KurumsalController@update");

Route::get("Magazalar","Dashboard\MagazalarController@index");
Route::post("Magazalar","Dashboard\MagazalarController@update");
Route::post("Magazalar/image","Dashboard\MagazalarController@updateImage");

Route::get("Contact","Dashboard\ContactController@index");
Route::post("Contact","Dashboard\ContactController@update");
Route::delete('Delete', 'Dashboard\HomeController@destroy');
});




Route::get('image/upload','ImageUploadController@fileCreate');
Route::post('image/upload/store','Dashboard\ImageUploadController@fileStore');
Route::post('image/delete','ImageUploadController@fileDestroy');

