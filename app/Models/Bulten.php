<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bulten extends Model
{
     public function up()
    {
        Schema::create('bultens', function (Blueprint $table) {
            $table->increments('id');
            $table->int('order');
            $table->timestamps();
        });
    }
}
