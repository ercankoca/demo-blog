<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Iletisim extends Model
{
    protected $table ="iletisim";

    protected $fillable = ["adres","telefon"];


}