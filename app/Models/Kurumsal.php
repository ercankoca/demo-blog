<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Kurumsal extends Model
{
    protected $table ="kurumsal";

    protected $fillable = ["title","content"];


}