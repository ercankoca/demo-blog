<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        $this->middleware('auth');
        $this->middleware('Isadmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       // return view('dashboard/dashboard');

         $user = User::find($userId);
    
    if($user->admin == 1)
    {
        return view('Dashboard/Dashboard');
    }
    return view('Dashboard/Dashboard');
    }

}
