<?php

namespace App\Http\Controllers\Dashboard;




use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kurumsal;
use Illuminate\Support\Facades\Input;

class KurumsalController extends Controller
{
    
    public function index()
    {
    	$rows=Kurumsal::all();

	    return view("Dashboard/Kurumsal")->with("rows",$rows);

    }

    public function update(Request $request)
    {

    	 $data = array(
         "content" => $request->content	,
        "title" => $request->title   );

        Kurumsal::where("id",$request->id)->update($data);
$rows=Kurumsal::all();

	    return view("Dashboard/Kurumsal")->with("rows",$rows);
    }
}
