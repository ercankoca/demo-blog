<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Models\ImageUpload;

class HomeController extends Controller
{
    public function index()
    {
    	return view("Dashboard/Dashboard");
    }



         public function destroy(Request $request)
{
    try 
    {
        //Veriler::whereIn('id', $request->id)->delete(); // $request->id MUST be an array
        ImageUpload::where('id', $request->id)->delete(); // $request->id MUST be an array
        //return response()->json('users deleted');

      
    }

    catch (Exception $e) {
        return response()->json($e->getMessage(), 500);
    }


        $ImageUpload=ImageUpload::orderBy('id','DESC')->get();
	    return view("Dashboard/ImageView")->with("Images",$ImageUpload);

}
}
