<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ImageUpload;
use Illuminate\Support\Facades\Input;
class ImageViewController extends Controller
{
	public function index()
	{
		//ImageUpload::orderBy('id','DESC')->get();
		$ImageUpload=ImageUpload::orderBy('id','DESC')->get();
	    return view("Dashboard/ImageView")->with("Images",$ImageUpload);
	}

	public function updatePage(Request $request)
	{
		 $id = input::get("id");

      
      $image = ImageUpload::where('id', '=',$request->id)->get();


	   	return view("Dashboard/ImageUpdate")->with("image",$image);

	}


	public function update(Request $request)
	{
		
		 $data = array(
         "filetitle" => $request->filetitle,
     	 "filecontent" => $request->filecontent,
     	 "price" => $request->price,
     	 "status" => $request->status,
     	 "price_old" => $request->price_old	);

        ImageUpload::where("id",$request->id)->update($data);


         $id = input::get("id");      
      $image = ImageUpload::where('id', '=',$request->id)->get();
      
       return view("Dashboard/ImageUpdate")->with("image",$image);

	}
}


