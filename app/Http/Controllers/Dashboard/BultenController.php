<?php

namespace App\Http\Controllers\Dashboard;




use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bulten;
use Illuminate\Support\Facades\Input;

class BultenController extends Controller
{
    
    public function index()
    {
    	$rows=Bulten::all();

	    return view("Dashboard/Bulten")->with("rows",$rows);

    }

    public function update(Request $request)
    {

    	 $data = array(
         "order" => $request->order,
     	 "first_date" => $request->first_date,
     	 "last_date" => $request->last_date	);

        Bulten::where("id",1)->update($data);
$rows=Bulten::all();

	    return view("Dashboard/Bulten")->with("rows",$rows);
    }
}
