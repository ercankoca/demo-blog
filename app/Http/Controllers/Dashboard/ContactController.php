<?php

namespace App\Http\Controllers\Dashboard;




use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Iletisim;
use Illuminate\Support\Facades\Input;

class ContactController extends Controller
{
    
    public function index()
    {
    	$rows=Iletisim::all();

	    return view("Dashboard/Iletisim")->with("rows",$rows);

    }

    public function update(Request $request)
    {

    	 $data = array(
         "adres" => $request->adres,
         "telefon" => $request->telefon	,
        "mail" => $request->mail  ,
        "website" => $request->website  );

        Iletisim::where("id",$request->id)->update($data);
$rows=Iletisim::all();

	    return view("Dashboard/Iletisim")->with("rows",$rows);
    }
}
