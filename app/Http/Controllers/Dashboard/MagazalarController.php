<?php

namespace App\Http\Controllers\Dashboard;




use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Magazalar;
use Illuminate\Support\Facades\Input;

class MagazalarController extends Controller
{
    
    public function index()
    {
    	$rows=Magazalar::all();

	    return view("Dashboard/Magazalar")->with("rows",$rows);

    }

    public function update(Request $request)
    {

    	 $data = array(
         "title" => $request->title,
         "content" => $request->content	);

        Magazalar::where("id",$request->id)->update($data);
$rows=Magazalar::all();

	    return view("Dashboard/Magazalar")->with("rows",$rows);
    }

     public function updateImage(Request $request)
    {

         $data = array(
         "image" => $request->image );

        Magazalar::where("id",$request->id)->update($data);
$rows=Magazalar::all();

        return view("Dashboard/Magazalar")->with("rows",$rows);
    }
}
