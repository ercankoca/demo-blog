<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class IsAdminController extends Controller
{


    public function handle($request, Closure $next)
{
    // Obtenir la liste des roles de l'utilisateur
    $UserRoles = DB::table('roles')->join('role_user','role_id', '=', 'roles.id')->where('user_id', '=', Auth::user()->id)->lists('name');
    // vérifier si cet utilisateur  a le role d'admin
    $isAdmin = false;
    foreach($UserRoles as $role)
    {
        if($role == 'admin')
        {
            $isAdmin = true;
        }
    }

    // snippet ci-dessous selon doc de Laravel
    if( ! $isAdmin )
    {
        if ($request->ajax()) {
            return response('Unauthorized.', 401);
        } else {
            return redirect()->back(); //todo h peut-etre une fenetre modale pour dire acces refusé ici...
        }
    }

    return $next($request);
}

}
